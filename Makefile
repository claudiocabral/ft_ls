# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/21 19:57:39 by claudioca         #+#    #+#              #
#    Updated: 2018/01/08 14:28:17 by ccabral          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
# jakd

NAME	:=	ft_ls
CC		:=	cc
CFLAGS	:=	-Wextra -Werror -Wall
CDEBUG	:=	-g

LIBFT_PATH	:=	libft/
PRINTF_PATH = 	ft_printf/

DEPDIR := .deps
$(shell mkdir -p $(DEPDIR) >/dev/null)
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
POSTCOMPILE = @mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.dep && touch $@

include $(LIBFT_PATH)/libft.mk
include $(PRINTF_PATH)/printf.mk

OBJS	=	objs/ft_parse_params.o \
			objs/calculate_nbr_columns.o \
			objs/sort_args.o \
			objs/make_entries.o \
			objs/make_path.o \
			objs/entry_info.o \
			objs/filemode_info.o \
			objs/owner_and_date.o \
			objs/ft_ls.o \
			objs/printer.o \
			objs/array_printer.o \
			objs/get_sort_functions.o \
			objs/ls_error.o \
			objs/get_colors.o \
			objs/main.o

INC	=	-Iincludes \
		-I$(LIBFT_INCLUDES) \
		-I$(PRINTF_INCLUDES)

ifeq ($(ASAN), 1)
	DEBUG := 1
	CDEBUG += -fsanitize=address
endif

ifeq ($(DEBUG), 1)
	CFLAGS	+=	$(CDEBUG)
endif

.PHONY: all clean fclean re

all: $(NAME)

include $(LIBFT_PATH)/libft_rules.mk
include $(PRINTF_PATH)/printf_rules.mk

$(NAME): $(OBJS) $(LIBFT) $(PRINTF)
	$(CC) $(CFLAGS) $(OBJS) $(INC) \
		-L$(LIBFT_PATH) -L$(PRINTF_PATH) -lft -lftprintf -o $@


objs/%.o: srcs/%.c Makefile
	@mkdir -p objs
	$(CC) -c $< -o $@ $(CFLAGS) $(DEPFLAGS) $(INC)
	$(POSTCOMPILE)

$(DEPDIR)/%.dep: ;
.PRECIOUS: $(DEPDIR)/%.dep
include $(wildcard $(OBJS:objs/%.o=$(DEPDIR)/%.dep))

fclean: clean
	make $(PRINTF_FCLEAN)
	make $(LIBFT_FCLEAN)
	rm -rf $(NAME)
	rm -rf $(DEPDIR)

clean:
	make $(LIBFT_CLEAN)
	make $(PRINTF_CLEAN)
	rm -rf objs

re:
	$(MAKE) fclean
	$(MAKE) all
