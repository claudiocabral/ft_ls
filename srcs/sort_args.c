/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_args.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 14:27:05 by ccabral           #+#    #+#             */
/*   Updated: 2018/01/08 17:04:46 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <ft_ls.h>

static int	is_directory(t_entry_info **a, t_entry_info **b)
{
	if (!a)
		return (1);
	else if (!b)
		return (-1);
	return (((*a)->type == 'd') - ((*b)->type == 'd'));
}

void		sort_args(t_entry_info **entries, t_params *params, int size)
{
	int	nbr_dirs;
	int	i;

	i = 0;
	nbr_dirs = 0;
	while (i < size)
	{
		if (entries[i]->type == 'd')
			++nbr_dirs;
		++i;
	}
	if (!params->flags.d)
	{
		ft_quicksort((void **)entries, 0, size - 1, (t_cmpf) & is_directory);
		ft_quicksort((void **)entries, 0, size - (nbr_dirs + 1),
														params->compare);
		ft_quicksort((void **)entries + (size - (nbr_dirs)), 0, nbr_dirs - 1,
														params->compare);
	}
	else
		ft_quicksort((void **)entries, 0, size - 1, params->compare);
}
