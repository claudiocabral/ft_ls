/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   owner_and_date.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 11:06:16 by claudioca         #+#    #+#             */
/*   Updated: 2017/11/29 11:08:17 by claudioca        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <pwd.h>
#include <time.h>
#include <grp.h>
#include <uuid/uuid.h>
#include <stdlib.h>
#include <ft_ls.h>

void	set_owner(char *owner, uid_t uid)
{
	struct passwd	*passwd;
	char			*tmp;

	passwd = getpwuid(uid);
	if (passwd && *(passwd->pw_name))
		ft_strlcpy(owner, passwd->pw_name, 32);
	else
	{
		tmp = ft_itoa(uid);
		ft_strlcpy(owner, tmp, 32);
		free(tmp);
	}
}

void	set_group(char *group_name, gid_t gid)
{
	struct group	*group;
	char			*tmp;

	group = getgrgid(gid);
	if (group && *(group->gr_name))
		ft_strlcpy(group_name, group->gr_name, 32);
	else
	{
		tmp = ft_itoa(gid);
		ft_strlcpy(group_name, tmp, 32);
		free(tmp);
	}
}

void	set_date(char date_modified[32], struct timespec date)
{
	char	*tmp;
	time_t	now;
	long	diff;

	now = time(0);
	diff = now - date.tv_sec;
	tmp = ctime(&(date.tv_sec));
	ft_strlcpy(date_modified, tmp + 4, 13);
	if (diff >= SIX_MONTHS)
	{
		date_modified[7] = ' ';
		date_modified[8] = tmp[20];
		date_modified[9] = tmp[21];
		date_modified[10] = tmp[22];
		date_modified[11] = tmp[23];
	}
}
