/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_path.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/03 12:19:24 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 11:36:21 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ls.h>
#include <libft.h>
#include <stdlib.h>

char			*make_path(const char *dir_path, const char *entry_path)
{
	int		size;
	char	*path;

	if (ft_strcmp(dir_path, "/") == 0)
		dir_path = "";
	size = ft_strlen(dir_path) + ft_strlen(entry_path) + 2;
	if (!(path = (char *)malloc(sizeof(char) * size)))
		return (0);
	*path = 0;
	ft_strcat(path, dir_path);
	ft_strcat(path, "/");
	ft_strcat(path, entry_path);
	return (path);
}
