/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_colors.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 21:48:50 by claudioca         #+#    #+#             */
/*   Updated: 2017/12/06 21:49:15 by claudioca        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ls.h>

char	*get_color(t_entry_info *entry)
{
	if (entry->type == 'd')
		return (GREEN);
	else if (entry->type == '-' && ft_strchr(entry->file_permissions, 'x'))
		return (RED);
	else if (entry->type == 'l')
		return (PURPLE);
	else if (entry->type == 'b')
		return (BLUE);
	else if (entry->type == 'c')
		return (DEV_COLOR);
	return (NO_COLOR);
}
