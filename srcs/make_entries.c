/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_entries.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 10:51:29 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 14:53:29 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/dir.h>
#include <stdlib.h>
#include <ft_ls.h>
#include <libft.h>

int				split_path(char const **path, char const **name, DIR **dir)
{
	if ((*dir = opendir(*path)))
	{
		*name = *path;
		return (1);
	}
	else if (!(*name = ft_strrchr(*path, '/')))
	{
		*name = *path;
		*dir = opendir(".");
	}
	else if (*name == *path)
	{
		++name;
		*dir = opendir("/");
	}
	else
	{
		**((char **)name) = 0;
		*dir = opendir(*path);
		**((char **)name) = '/';
		++(*name);
	}
	return (0);
}

DIR				*get_entry(char const *path, t_dirent **entry)
{
	DIR			*dir;
	char const	*name;

	if (split_path(&path, &name, &dir))
	{
		*entry = readdir(dir);
		return (dir);
	}
	while ((*entry = readdir(dir)))
		if (ft_strcmp((*entry)->d_name, name) == 0)
			break ;
	return (dir);
}

t_entry_info	*make_argv_entry(char const *path, t_flags *flags)
{
	t_entry_info	*entry_info;
	t_dirent		*entry;
	DIR				*dir;

	if (!(dir = get_entry(path, &entry)) || !entry
			|| !(entry_info = (t_entry_info *)malloc(sizeof(t_entry_info)))
			|| !(entry_info->path = ft_strdup(path)))
		return ((t_entry_info *)handle_dir_error(path));
	entry_info->base_name = entry_info->path;
	fill_entry_info(entry_info, flags);
	closedir(dir);
	return (entry_info);
}

t_entry_info	*make_entry_info(t_dirent *entry, char const *path,
													t_flags *flags)
{
	t_entry_info	*entry_info;

	if (!(entry_info = (t_entry_info *)malloc(sizeof(t_entry_info)))
			|| !(entry_info->path = make_path(path, entry->d_name)))
		return (0);
	entry_info->base_name = ft_strrchr(entry_info->path, '/') + 1;
	fill_entry_info(entry_info, flags);
	return (entry_info);
}

t_entry_info	**make_sorted_args(int argc, char **argv, t_params *params)
{
	t_entry_info	**entries;
	int				i;
	int				j;

	if (!(entries = (t_entry_info **)malloc(sizeof(t_entry_info *)
														* (argc + 1))))
		return (0);
	i = 0;
	while (i < argc + 1)
		entries[i++] = 0;
	i = 0;
	j = 0;
	while (i < argc)
	{
		entries[j] = make_argv_entry(argv[i], &params->flags);
		++i;
		if (entries[j] != 0)
			++j;
	}
	sort_args(entries, params, j);
	return (entries);
}
