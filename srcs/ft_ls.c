/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 19:49:03 by ccabral           #+#    #+#             */
/*   Updated: 2018/01/08 14:58:09 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <stdlib.h>
#include <libft.h>
#include <array.h>
#include <ft_printf.h>
#include <ft_ls.h>
#include <unistd.h>

static void	recurse_dirs(t_entry_info **entry_ptr, t_params *params)
{
	t_entry_info *entry;

	entry = *entry_ptr;
	if (entry->type == 'd' && entry->base_name[0] != '.')
	{
		ft_printf("\n%s:\n", entry->path);
		do_ls(entry, params);
	}
}

int			do_ls(t_entry_info *entry, t_params *params)
{
	t_array			*folder;

	if (entry->type != 'd')
	{
		params->print(&entry, params);
		return (0);
	}
	if (!(folder = array_create(sizeof(void *), 64)))
		return (1);
	if (!fill_dir_info(folder, entry->path, params))
		return (1);
	if (params->flags.l && folder->end != folder->begin)
		ft_printf("total %d\n", params->total_block_count);
	params->apply(folder, params, params->print);
	reset_params(params);
	if (params->flags.recurse)
	{
		if (params->flags.r)
			array_apply_reverse(folder, params, (t_applyf) & recurse_dirs);
		else
			array_apply(folder, params, (t_applyf) & recurse_dirs);
	}
	array_free(folder, (t_freef) & free_entry_info);
	return (0);
}

int			print_entries(t_entry_info **entries, t_params *params, int size)
{
	t_array	dummy;
	int		i;

	dummy.element_size = sizeof(t_entry_info *);
	dummy.begin = entries;
	i = 0;
	while (entries[i] && i < size)
	{
		collect_global_data(params, entries[i]);
		++i;
	}
	dummy.end = entries + i;
	params->nbr_rows = i;
	calculate_nbr_columns(params);
	params->apply(&dummy, params, params->print);
	i = 0;
	while (i < size && entries[i])
		free_entry_info(entries[i++]);
	return (1);
}

int			ft_ls(t_entry_info **entries, t_params *params)
{
	int				i;
	int				ret;

	i = 0;
	ret = 0;
	while (entries[i])
	{
		if (entries[i]->type == 'd')
			ft_printf("%s:\n", entries[i]->path);
		if (do_ls(entries[i], params) == 0)
		{
			if (entries[i + 1] && entries[i + 1]->type == 'd')
				write(1, "\n", 1);
		}
		else
			ret = 1;
		free_entry_info(entries[i++]);
	}
	return (ret);
}
