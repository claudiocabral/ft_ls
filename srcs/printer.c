/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/27 17:18:06 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/10 11:32:43 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <sys/xattr.h>
#include <ft_printf.h>
#include <errno.h>
#include <ft_ls.h>

void		print_entry(t_entry_info **entry_ptr, t_params *params)
{
	t_entry_info *entry;

	entry = *entry_ptr;
	ft_printf("%s%s%s\n",
			params->flags.colors ? get_color(entry) : "",
			entry->base_name,
			params->flags.colors ? NO_COLOR : "");
}

static void	print_extended_attributes(t_entry_info *entry)
{
	char	buffer[1025];
	ssize_t	size;
	ssize_t	offset;
	int		i;

	offset = 0;
	i = 0;
	while (offset < entry->nbr_extended_attributes)
	{
		size = getxattr(entry->path,
				entry->extended_attributes + offset,
				(void *)buffer, 1025, 0, XATTR_NOFOLLOW);
		ft_printf("\t%s\t% 6d \n", entry->extended_attributes + offset, size);
		offset += ft_strlen(entry->extended_attributes + offset) + 1;
	}
}

static void	print_long_block(t_entry_info *entry, t_params *params)
{
	ft_printf("%c%s %*d %*s%s%*s  %3d, %3d %s %s%s%s\n",
			entry->type,
			entry->file_permissions,
			params->links_field_width,
			entry->nbr_links,
			params->flags.g ? 0 : params->owner_field_width * -1,
			params->flags.g ? "" : entry->owner,
			params->flags.g ? "" : "  ",
			params->group_field_width * -1,
			entry->group,
			entry->device_major,
			entry->device_minor,
			entry->date_modified,
			params->flags.colors ? get_color(entry) : "",
			entry->base_name,
			params->flags.colors ? NO_COLOR : "");
}

void		print_long_entry(t_entry_info **entry_ptr, t_params *params)
{
	t_entry_info *entry;

	entry = *entry_ptr;
	if (entry->type == 'b' || entry->type == 'c')
		print_long_block(entry, params);
	else if (entry->base_name[0] != '.' || params->flags.a || params->flags.d)
	{
		ft_printf("%c%s %*d %*s%s%*s  %*d %s %s%s%s%s%s\n",
				entry->type, entry->file_permissions,
				params->links_field_width, entry->nbr_links,
				params->flags.g ? 0 : params->owner_field_width * -1,
				params->flags.g ? "" : entry->owner,
				params->flags.g ? "" : "  ",
				params->group_field_width * -1, entry->group,
				params->block_field_width, entry->size,
				entry->date_modified,
				params->flags.colors ? get_color(entry) : "",
				entry->base_name,
				params->flags.colors ? NO_COLOR : "",
				entry->type == 'l' ? " -> " : "",
				entry->link_contents);
		if (entry->nbr_extended_attributes && params->flags.extended)
			print_extended_attributes(entry);
	}
}

t_printer	get_printer_function(t_flags *flags)
{
	if (flags->l)
		return (&print_long_entry);
	if (flags->one)
		return (&print_entry);
	return (&print_columns);
}
