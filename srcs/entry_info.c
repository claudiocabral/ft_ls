/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   entry_info.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 14:28:41 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 11:34:31 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <sys/dir.h>
#include <stdlib.h>
#include <stdio.h>
#include <ft_ls.h>
#include <libft.h>
#include <array.h>
#include <unistd.h>

void	set_device_info(t_entry_info *entry, dev_t rdev)
{
	entry->device_major = rdev >> 24;
	entry->device_minor = rdev & 0xff;
}

void	fill_entry_info(t_entry_info *entry_info, t_flags *flags)
{
	struct stat		buf;
	ssize_t			size;

	lstat(entry_info->path, &buf);
	size = 0;
	entry_info->type = get_type(buf.st_mode);
	if (entry_info->type == 'l')
		size = readlink(entry_info->path, entry_info->link_contents, BUFF_SIZE);
	entry_info->link_contents[size] = 0;
	fill_permissions(entry_info, buf.st_mode);
	entry_info->nbr_links = buf.st_nlink;
	set_owner(entry_info->owner, buf.st_uid);
	set_group(entry_info->group, buf.st_gid);
	entry_info->size = buf.st_size;
	entry_info->block_count = buf.st_blocks;
	if (flags->u)
		set_date(entry_info->date_modified, buf.st_atimespec);
	else
		set_date(entry_info->date_modified, buf.st_mtimespec);
	entry_info->n_date_modified = buf.st_mtimespec.tv_sec;
	entry_info->n_date_accessed = buf.st_atimespec.tv_sec;
	set_device_info(entry_info, buf.st_rdev);
}

void	collect_global_data(t_params *params, t_entry_info *info)
{
	if (info->base_name[0] != '.' || params->flags.a || params->flags.d)
	{
		params->total_block_count += info->block_count;
		params->name_field_width = ft_max(params->name_field_width,
				ft_strlen(info->base_name));
		params->links_field_width = ft_max(params->links_field_width,
				ft_nbr_digits(info->nbr_links));
		params->block_field_width = ft_max(params->block_field_width,
				ft_nbr_digits(info->size));
		params->owner_field_width = ft_max(params->owner_field_width,
				ft_strlen(info->owner));
		params->group_field_width =
			ft_max(params->group_field_width, ft_strlen(info->group));
		params->has_device = info->type == 'c' || info->type == 'b';
	}
}

int		fill_dir_info(t_array *folder, char const *path,
		t_params *params)
{
	DIR				*dir;
	t_dirent		*entry;
	t_entry_info	*it;
	int				ret;

	ret = 1;
	if (!(dir = opendir(path)))
		return (handle_dir_error(path));
	while ((entry = readdir(dir)))
	{
		if (entry->d_name[0] == '.' && !params->flags.a && !params->flags.d)
			continue ;
		it = make_entry_info(entry, path, &params->flags);
		if (!array_push_back(folder, &it))
			return (0);
		collect_global_data(params, it);
		params->nbr_rows++;
	}
	params->column = 0;
	calculate_nbr_columns(params);
	if (params->has_device)
		params->block_field_width += 8 - params->block_field_width;
	closedir(dir);
	array_sort(folder, params->compare);
	return (ret);
}

void	free_entry_info(t_entry_info *ptr)
{
	if (ptr)
		free((char *)ptr->path);
	free(ptr);
}
