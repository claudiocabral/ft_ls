/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 11:32:10 by ccabral           #+#    #+#             */
/*   Updated: 2018/01/08 15:06:50 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <libft.h>
#include <ft_ls.h>
#include <unistd.h>
#include <stdlib.h>

int	print_file_entries(t_entry_info **entries, t_params *params)
{
	int	i;

	i = 0;
	while (entries[i])
	{
		if (!params->flags.d && entries[i]->type == 'd')
			break ;
		++i;
	}
	if (i)
		print_entries(entries, params, i);
	if (i != 0 && entries[i])
		ft_putchar('\n');
	return (i);
}

int	main(int argc, char **argv)
{
	t_params		params;
	t_entry_info	**entries;
	int				ret;

	ft_parse_params(&argc, &argv, &params);
	if (argc == 0)
	{
		argc = 1;
		argv[0] = ".";
	}
	if (!(entries = make_sorted_args(argc, argv, &params)) ||
			!entries[0])
		return (1);
	ret = print_file_entries(entries, &params);
	if (!ret && argc == 1)
	{
		ret = ft_min(ret, do_ls(entries[ret], &params));
		free_entry_info(entries[argc - 1]);
	}
	else
		ret = ft_min(ret, ft_ls(entries + ret, &params));
	free(entries);
	return (ret < 0 ? 1 : 0);
}
