/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate_nbr_columns.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 23:20:37 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 11:27:58 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ls.h>
#include <sys/ioctl.h>
#include <ft_printf.h>

void	calculate_nbr_columns(t_params *params)
{
	struct winsize	window;

	ioctl(0, TIOCGWINSZ, &window);
	if (!window.ws_col)
		params->nbr_columns = 1;
	else
		params->nbr_columns = window.ws_col
			/ ((params->name_field_width + 8) / 8 * 8);
	if (!params->nbr_columns)
		params->nbr_columns++;
	params->nbr_rows = params->nbr_rows / params->nbr_columns
		+ ((params->nbr_rows % params->nbr_columns) != 0);
}
