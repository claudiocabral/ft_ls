/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 14:34:39 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 14:33:39 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <ft_ls.h>
#include <stdlib.h>
#include <errno.h>

size_t	handle_dir_error(char const *path)
{
	ft_dprintf(2, "ft_ls: %s: %s\n", path, strerror(errno));
	return (0);
}

void	handle_flag_error(char flag)
{
	ft_dprintf(2, "ft_ls: illegal options -- %c\n", flag);
	ft_dprintf(2, "usage: ft_ls [-@GRadfglrtu] [file ...]\n");
	exit(1);
}
