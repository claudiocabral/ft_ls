/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filemode_info.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 10:59:20 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/03 13:56:22 by claudioca        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/acl.h>
#include <sys/stat.h>
#include <sys/xattr.h>
#include <ft_ls.h>

char	get_extended_attributes(t_entry_info *info)
{
	acl_t	acl;
	char	c;

	c = ' ';
	info->nbr_extended_attributes =
		listxattr(info->path, (char *)info->extended_attributes, BUFF_SIZE,
				XATTR_NOFOLLOW);
	acl = acl_get_file(info->path, ACL_TYPE_EXTENDED);
	if (acl)
	{
		c = '+';
		acl_free(acl);
	}
	return (info->nbr_extended_attributes > 0 ? '@' : c);
}

void	fill_permissions(t_entry_info *info, mode_t mode)
{
	info->file_permissions[0] = (mode & S_IRUSR) ? 'r' : '-';
	info->file_permissions[1] = (mode & S_IWUSR) ? 'w' : '-';
	info->file_permissions[2] = (mode & S_IXUSR) ? 'x' : '-';
	if (info->file_permissions[2] == 'x')
		info->file_permissions[2] = (mode & S_ISUID) ? 's' : 'x';
	else
		info->file_permissions[2] = (mode & S_ISUID) ? 'S' : '-';
	info->file_permissions[3] = (mode & S_IRGRP) ? 'r' : '-';
	info->file_permissions[4] = (mode & S_IWGRP) ? 'w' : '-';
	info->file_permissions[5] = (mode & S_IXGRP) ? 'x' : '-';
	if (info->file_permissions[5] == 'x')
		info->file_permissions[5] = (mode & S_ISGID) ? 's' : 'x';
	else
		info->file_permissions[5] = (mode & S_ISGID) ? 'S' : '-';
	info->file_permissions[6] = (mode & S_IROTH) ? 'r' : '-';
	info->file_permissions[7] = (mode & S_IWOTH) ? 'w' : '-';
	info->file_permissions[8] = (mode & S_IXOTH) ? 'x' : '-';
	if (info->file_permissions[8] == 'x')
		info->file_permissions[8] = (mode & S_ISVTX) ? 't' : 'x';
	else
		info->file_permissions[8] = (mode & S_ISVTX) ? 'T' : '-';
	info->file_permissions[9] = get_extended_attributes(info);
	info->file_permissions[10] = '\0';
}

char	get_type(mode_t mode)
{
	if ((mode & S_IFMT) == S_IFBLK)
		return ('b');
	else if ((mode & S_IFMT) == S_IFCHR)
		return ('c');
	else if ((mode & S_IFMT) == S_IFDIR)
		return ('d');
	else if ((mode & S_IFMT) == S_IFLNK)
		return ('l');
	else if ((mode & S_IFMT) == S_IFSOCK)
		return ('s');
	else if ((mode & S_IFMT) == S_IFIFO)
		return ('p');
	return ('-');
}
