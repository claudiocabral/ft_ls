/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_sort_functions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 21:37:27 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 11:35:45 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ls.h>
#include <libft.h>

int		last_access(t_entry_info **a, t_entry_info **b)
{
	int	diff;

	if (!a || !b)
		return (a < b);
	if ((diff = (*b)->n_date_accessed - (*a)->n_date_accessed) == 0)
		return (ft_strcmp((*a)->base_name, (*b)->base_name));
	return (diff);
}

int		time_modified(t_entry_info **a, t_entry_info **b)
{
	int	diff;

	if (!a || !b)
		return (a < b);
	if ((diff = (*b)->n_date_modified - (*a)->n_date_modified) == 0)
		return (ft_strcmp((*a)->base_name, (*b)->base_name));
	return (diff);
}

int		always_true(t_entry_info **a, t_entry_info **b)
{
	(void)a;
	(void)b;
	return (0);
}

int		lexicographical_compare(t_entry_info **a, t_entry_info **b)
{
	if (!a)
		return (1);
	else if (!b)
		return (-1);
	return (ft_strcmp((*a)->base_name, (*b)->base_name));
}

t_cmpf	get_comparison_function(t_flags *flags)
{
	if (flags->f)
	{
		flags->r = 0;
		flags->a = 1;
		return ((t_cmpf)&always_true);
	}
	else if (flags->u && flags->t)
		return ((t_cmpf)&last_access);
	else if (flags->t)
		return ((t_cmpf)&time_modified);
	return ((t_cmpf)&lexicographical_compare);
}
