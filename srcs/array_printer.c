/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_printer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/03 12:22:10 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 13:20:47 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ls.h>
#include <ft_printf.h>

void		print_columns(t_entry_info **entry_ptr, t_params *params)
{
	t_entry_info	*entry;

	entry = *entry_ptr;
	ft_printf("%s%*s%s%c",
			params->flags.colors ? get_color(entry) : "",
			-1 * (params->column != params->nbr_columns - 1 ?
			params->name_field_width : ft_strlen(entry->base_name)),
			entry->base_name,
			params->flags.colors ? NO_COLOR : "",
			params->column != params->nbr_columns - 1 ? '\t' : '\n');
}

void		array_print_columns_reverse(t_array *array, t_params *params,
														t_printer print)
{
	t_entry_info	**entry;
	int				i;
	int				j;
	int				size;

	entry = array->begin;
	size = ((array->end - array->begin) / array->element_size);
	i = size - 1;
	size -= params->nbr_rows + 1;
	while (i > size)
	{
		j = 0;
		while (i - (j * params->nbr_rows) >= 0)
		{
			if (i - (j + 1) * params->nbr_rows < 0)
				params->column = params->nbr_columns - 1;
			else
				params->column = 0;
			print(entry + i - j * params->nbr_rows, params);
			++j;
		}
		--i;
	}
}

void		array_print_columns(t_array *array, t_params *params,
		t_printer print)
{
	t_entry_info	**entry;
	int				i;
	int				j;
	int				size;

	entry = array->begin;
	size = ((array->end - array->begin) / array->element_size);
	i = 0;
	while (i < params->nbr_rows)
	{
		j = 0;
		while (i + (j * params->nbr_rows) < size)
		{
			if (i + (j + 1) * params->nbr_rows >= size)
				params->column = params->nbr_columns - 1;
			else
				params->column = 0;
			print(entry + i + j * params->nbr_rows, params);
			++j;
		}
		++i;
	}
}
