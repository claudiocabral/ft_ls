/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/28 14:44:19 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 14:58:05 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <ft_ls.h>
#include <ft_printf.h>

static int		continue_set_flags(char c, t_flags *flags)
{
	if (c == 't')
		flags->t = 1;
	else if (c == 'u')
		flags->u = 1;
	else if (c == 'G')
		flags->colors = 1;
	else if (c == '@')
		flags->extended = 1;
	else if (c == '1')
	{
		flags->one = 1;
		flags->l = 0;
	}
	else
		return (0);
	return (1);
}

static int		set_ls_flags(char c, t_flags *flags)
{
	if (c == 'a')
		flags->a = 1;
	else if (c == 'd')
		flags->d = 1;
	else if (c == 'f')
		flags->f = 1;
	else if (c == 'g')
		flags->g = 1;
	else if (c == 'l')
	{
		flags->one = 0;
		flags->l = 1;
	}
	else if (c == 'r')
		flags->r = 1;
	else if (c == 'R')
		flags->recurse = 1;
	else
		return (continue_set_flags(c, flags));
	return (1);
}

void			reset_params(t_params *params)
{
	params->nbr_rows = 0;
	params->nbr_columns = 0;
	params->name_field_width = 0;
	params->owner_field_width = 0;
	params->group_field_width = 0;
	params->block_field_width = 0;
	params->has_device = 0;
	params->links_field_width = 0;
	params->total_block_count = 0;
}

static void		set_params(t_params *params)
{
	params->compare = get_comparison_function(&params->flags);
	params->print = get_printer_function(&params->flags);
	if (params->flags.r)
		params->apply = &array_apply_reverse;
	else
		params->apply = &array_apply;
	if (params->print == &print_columns)
	{
		if (params->flags.r)
			params->apply = (t_apply_order) & array_print_columns_reverse;
		else
			params->apply = (t_apply_order) & array_print_columns;
	}
	reset_params(params);
}

void			ft_parse_params(int *argc, char ***argv, t_params *params)
{
	int		i;
	int		j;

	ft_bzero((void *)&params->flags, sizeof(t_flags));
	i = 1;
	while (i < *argc && (*argv)[i][0] == '-' && (*argv)[i][1])
	{
		if (ft_strcmp((*argv)[i], "--") == 0)
		{
			++i;
			break ;
		}
		j = 1;
		while ((*argv)[i][j])
		{
			if (!(set_ls_flags((*argv)[i][j], &(params->flags))))
				handle_flag_error((*argv)[i][j]);
			++j;
		}
		++i;
	}
	set_params(params);
	*argc -= i;
	*argv += i;
}
