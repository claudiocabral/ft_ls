/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 16:06:40 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 15:52:45 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <array.h>

t_array			*array_create(size_t element_size, size_t nbr_elements)
{
	t_array	*array;

	if (!(array = malloc(sizeof(t_array))))
		return (0);
	array->capacity = element_size * nbr_elements;
	if (!(array->begin = malloc(array->capacity + element_size)))
	{
		free(array);
		return (0);
	}
	array->end = (unsigned char *)array->begin;
	array->element_size = element_size;
	ft_bzero(array->begin, array->capacity + element_size);
	return (array);
}

void			*median(void *begin, void *end, size_t size)
{
	return (begin + ((size_t)(end - begin) / (size * 2)) * size);
}

void			*array_push_back(t_array *array, void *element)
{
	if (array->capacity <= array->end - array->begin + array->element_size
										&& !array_increase_capacity(array))
		return (0);
	ft_memcpy(array->end, element, array->element_size);
	array->end = (unsigned char *)array->end + array->element_size;
	ft_bzero(array->end, array->element_size);
	return ((unsigned char *)array->end - array->element_size);
}

void			*array_insert(t_array *array, void *where, void *element)
{
	if (array->capacity <= array->end - array->begin + array->element_size
										&& !array_increase_capacity(array))
		return (0);
	ft_memmove(where + array->element_size, where, array->end - where);
	ft_memcpy(where, element, array->element_size);
	array->end += array->element_size;
	ft_bzero(array->end, array->element_size);
	return (where);
}
