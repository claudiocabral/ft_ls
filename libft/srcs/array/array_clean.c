/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_clean.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccabral <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 15:09:16 by ccabral           #+#    #+#             */
/*   Updated: 2018/01/08 15:11:11 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <array.h>

void			array_clear(t_array *array, t_freef free_func)
{
	void	**it;

	it = array->begin;
	while (it != array->end)
	{
		free_func(*it);
		ft_bzero(it, array->element_size);
		it = it + array->element_size;
	}
	array->end = array->begin;
}

void			array_free(t_array *array, t_freef free_func)
{
	void	**it;

	it = array->begin;
	while (it != array->end)
	{
		free_func(*it);
		it = (void *)it + array->element_size;
	}
	free(array->begin);
	free(array);
}

void			array_remove(t_array *array, void *element, t_freef freef)
{
	freef(element);
	ft_memmove(element, element + array->element_size,
						array->end - element);
	array->end -= array->element_size;
}

void			array_remove_if(t_array *array, void const *data,
											t_freef freef, t_predf predicate)
{
	void	*it;

	it = array->begin;
	while (it != array->end)
	{
		if (predicate(it, data))
		{
			array_remove(array, it, freef);
			continue ;
		}
		it += array->element_size;
	}
}

int				array_increase_capacity(t_array *array)
{
	void	*buffer;

	if (!(buffer = malloc(array->capacity * 2)))
		return (0);
	ft_memcpy(buffer, array->begin, array->capacity);
	free(array->begin);
	array->end = buffer + (array->end - array->begin);
	array->begin = buffer;
	array->capacity += array->capacity;
	return (1);
}
