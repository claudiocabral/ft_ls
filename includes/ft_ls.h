/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: claudiocabral <cabral1349@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 14:04:54 by claudioca         #+#    #+#             */
/*   Updated: 2018/01/08 14:57:44 by ccabral          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H
# define STR(x) #x
# define RED STR(\033[0;31m)
# define GREEN STR(\033[1;32m)
# define BLUE STR(\033[1;34m)
# define PURPLE STR(\033[0;35m)
# define DEV_COLOR STR(\033[0;43m\033[1;32m)
# define NO_COLOR STR(\033[0m)
# define SIX_MONTHS 15724800
# define BUFF_SIZE 1024

# include <libft.h>
# include <array.h>
# include <sys/ioctl.h>
# include <sys/stat.h>

typedef void			(*t_apply_order)(t_array *array, void *args,
														t_applyf func);

typedef struct			s_entry_info
{
	char			type;
	char			file_permissions[11];
	int				nbr_links;
	char			owner[32];
	char			group[32];
	int				size;
	int				block_count;
	int				n_date_modified;
	int				n_date_accessed;
	char			date_modified[32];
	char const		*path;
	char const		*base_name;
	char			link_contents[BUFF_SIZE + 1];
	int				device_major;
	int				device_minor;
	ssize_t			nbr_extended_attributes;
	char			extended_attributes[BUFF_SIZE + 1];
}						t_entry_info;

typedef int				t_bool;
typedef struct dirent	t_dirent;
typedef void			(*t_printer)();

typedef struct			s_flags
{
	t_bool	a : 1;
	t_bool	d : 1;
	t_bool	f : 1;
	t_bool	g : 1;
	t_bool	l : 1;
	t_bool	r : 1;
	t_bool	recurse : 1;
	t_bool	t : 1;
	t_bool	u : 1;
	t_bool	one : 1;
	t_bool	colors : 1;
	t_bool	extended : 1;
}						t_flags;

typedef struct			s_params
{
	t_cmpf			compare;
	t_apply_order	apply;
	t_flags			flags;
	t_printer		print;
	int				name_field_width;
	int				total_block_count;
	int				owner_field_width;
	int				group_field_width;
	int				block_field_width;
	int				has_device;
	int				links_field_width;
	int				column;
	int				nbr_columns;
	int				nbr_rows;
}						t_params;

void					ft_parse_params(int *argc, char ***argv,
													t_params *params);
void					reset_params(t_params *params);
int						ft_ls(t_entry_info **entries, t_params *params);
void					print_entry(t_entry_info **entry_ptr, t_params *params);
void					print_long_entry(t_entry_info **entry, t_params
																	*params);
void					delete_path(void *path, size_t size);
t_cmpf					get_comparison_function(t_flags *flags);
t_printer				get_printer_function(t_flags *flags);
char					*make_path(const char *dir_path, const char
																*entry_path);
t_entry_info			*make_argv_entry(char const *path, t_flags *flags);
int						fill_dir_info(t_array *folder, char const *path,
														t_params *params);
void					fill_entry_info(t_entry_info *entry_info,
													t_flags *flags);
void					free_entry_info(t_entry_info *ptr);
t_entry_info			*make_entry_info(t_dirent *entry, char const *path,
															t_flags *flags);
t_entry_info			**make_sorted_args(int argc, char **argv,
												t_params *params);
void					handle_flag_error(char flag);
size_t					handle_dir_error(char const *path);
char					get_extended_attributes(t_entry_info *info);
void					fill_permissions(t_entry_info *info, mode_t mode);
char					get_type(mode_t mode);
void					set_owner(char *owner, uid_t uid);
void					set_group(char *group_name, gid_t gid);
void					set_date(char date_modified[32], struct timespec date);
int						do_ls(t_entry_info *entry, t_params *params);
char					*get_color(t_entry_info *entry);
void					calculate_nbr_columns(t_params *params);
void					print_columns(t_entry_info **entry, t_params *params);
void					array_print_columns(t_array *array, t_params *params,
															t_printer print);
void					array_print_columns_reverse(t_array *array,
										t_params *params, t_printer print);
void					collect_global_data(t_params *params,
														t_entry_info *info);
int						print_entries(t_entry_info **entries,
												t_params *params, int size);
void					sort_args(t_entry_info **entries,
												t_params *params, int size);

#endif
